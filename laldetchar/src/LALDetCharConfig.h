/* src/LALDetCharConfig.h.  Generated from LALDetCharConfig.h.in by configure.  */
/* only include this file if LALDetChar's config.h has not been included */
#ifndef LALDETCHAR_VERSION

/* LALDetChar Version */
#define LALDETCHAR_VERSION "0.3.5.1"

/* LALDetChar Version Major Number  */
#define LALDETCHAR_VERSION_MAJOR 0

/* LALDetChar Version Minor Number  */
#define LALDETCHAR_VERSION_MINOR 3

/* LALDetChar Version Micro Number  */
#define LALDETCHAR_VERSION_MICRO 5

/* LALDetChar Version Devel Number  */
#define LALDETCHAR_VERSION_DEVEL 1

#endif /* LALDETCHAR_VERSION */
