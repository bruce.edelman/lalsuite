/* src/config.h.  Generated from config.h.in by configure.  */
/* src/config.h.in.  Generated from configure.ac by autoheader.  */

/* Define to 1 to use GSL C99 inline code */
/* #undef GSL_C99_INLINE */

/* Define to 1 to turn GSL range checking off */
/* #undef GSL_RANGE_CHECK_OFF */

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define to 1 if you have the <glib.h> header file. */
#define HAVE_GLIB_H 1

/* Define to 1 if you have the <gsl/gsl_complex.h> header file. */
/* #undef HAVE_GSL_GSL_COMPLEX_H */

/* Define to 1 if you have the <gsl/gsl_errno.h> header file. */
#define HAVE_GSL_GSL_ERRNO_H 1

/* Define to 1 to use inline code */
/* #undef HAVE_INLINE */

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the <lal/EPSearch.h> header file. */
/* #undef HAVE_LAL_EPSEARCH_H */

/* Define to 1 if you have the <lal/LALCache.h> header file. */
/* #undef HAVE_LAL_LALCACHE_H */

/* Define to 1 if you have the <lal/LALSimulation.h> header file. */
/* #undef HAVE_LAL_LALSIMULATION_H */

/* Define to 1 if you have the <lal/LIGOLwXMLRead.h> header file. */
/* #undef HAVE_LAL_LIGOLWXMLREAD_H */

/* Define to 1 if you have the <lal/XLALError.h> header file. */
/* #undef HAVE_LAL_XLALERROR_H */

/* Define to 1 if you have the `glib-2.0' library (-lglib-2.0). */
#define HAVE_LIBGLIB_2_0 1

/* Define to 1 if you have the `gsl' library (-lgsl). */
#define HAVE_LIBGSL 1

/* Define to 1 if you have the LAL library */
#define HAVE_LIBLAL 1

/* Define to 1 if you have the LALBurst library */
#define HAVE_LIBLALBURST 1

/* Define to 1 if you have the LALMetaIO library */
#define HAVE_LIBLALMETAIO 1

/* Define to 1 if you have the LALSimulation library */
#define HAVE_LIBLALSIMULATION 1

/* Define to 1 if you have the LALSupport library */
#define HAVE_LIBLALSUPPORT 1

/* Define to 1 if you have the `m' library (-lm). */
#define HAVE_LIBM 1

/* Define to 1 if you have the `metaio' library (-lmetaio). */
#define HAVE_LIBMETAIO 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* LALDetChar Version */
#define LALDETCHAR_VERSION "0.3.5.1"

/* LALDetChar Version Devel Number */
#define LALDETCHAR_VERSION_DEVEL 1

/* LALDetChar Version Major Number */
#define LALDETCHAR_VERSION_MAJOR 0

/* LALDetChar Version Micro Number */
#define LALDETCHAR_VERSION_MICRO 5

/* LALDetChar Version Minor Number */
#define LALDETCHAR_VERSION_MINOR 3

/* Define to the sub-directory where libtool stores uninstalled libraries. */
#define LT_OBJDIR ".libs/"

/* Name of package */
#define PACKAGE "laldetchar"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "lal-discuss@ligo.org"

/* Define to the full name of this package. */
#define PACKAGE_NAME "LALDetChar"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "LALDetChar 0.3.5.1"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "laldetchar"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "0.3.5.1"

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Version number of package */
#define VERSION "0.3.5.1"
