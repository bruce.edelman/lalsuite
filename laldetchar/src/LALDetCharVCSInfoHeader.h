/*
 * Copyright (C) 2014, 2016 Karl Wette
 * Copyright (C) 2009-2013 Adam Mercer
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with with program; see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * LALDetCharVCSInfo.h - LALDetChar VCS Information Header
 */

/** \cond DONT_DOXYGEN */

#ifndef _LALDETCHARVCSINFOHEADER_H
#define _LALDETCHARVCSINFOHEADER_H

#include <lal/LALVCSInfoType.h>
#include <lal/LALDetCharVCSInfo.h>
#include <lal/LALDetCharConfig.h>

#ifdef __cplusplus
extern "C" {
#endif

/* VCS information */
#define LALDETCHAR_VCS_ID "75a9b2b52e6d98ea4647b86c80018245ee66bda9"
#define LALDETCHAR_VCS_DATE "2019-03-18 19:45:02 +0000"
#define LALDETCHAR_VCS_BRANCH "signal_spline_latest"
#define LALDETCHAR_VCS_TAG "None"
#define LALDETCHAR_VCS_AUTHOR "Bruce Edelman <bruce.edelman@ligo.org>"
#define LALDETCHAR_VCS_COMMITTER "Bruce Edelman <bruce.edelman@ligo.org>"
#define LALDETCHAR_VCS_CLEAN "UNCLEAN"
#define LALDETCHAR_VCS_STATUS "UNCLEAN: Modified working tree"

/* VCS header/library mismatch link check function */
#define LALDETCHAR_VCS_LINK_CHECK LALDETCHAR_UNCLEAN_75a9b2b52e6d98ea4647b86c80018245ee66bda9_VCS_HEADER_LIBRARY_MISMATCH
void LALDETCHAR_VCS_LINK_CHECK(void);

#ifdef __cplusplus
}
#endif

#endif /* _LALDETCHARVCSINFOHEADER_H */

/** \endcond */
