# source this file to access LALDetChar
export PYTHONPATH
PYTHONPATH=`echo "$PYTHONPATH" | /bin/sed -e 's|/home/bedelman/opt/lalsuite/lib/python2.7/site-packages:||g;'`
PYTHONPATH="/home/bedelman/opt/lalsuite/lib/python2.7/site-packages:$PYTHONPATH"
export MANPATH
MANPATH=`echo "$MANPATH" | /bin/sed -e 's|/home/bedelman/opt/lalsuite/share/man:||g;'`
MANPATH="/home/bedelman/opt/lalsuite/share/man:$MANPATH"
export LALDETCHAR_DATADIR
LALDETCHAR_DATADIR="/home/bedelman/opt/lalsuite/share/laldetchar"
export LALDETCHAR_PREFIX
LALDETCHAR_PREFIX="/home/bedelman/opt/lalsuite"
export PATH
PATH=`echo "$PATH" | /bin/sed -e 's|/home/bedelman/opt/lalsuite/bin:||g;'`
PATH="/home/bedelman/opt/lalsuite/bin:$PATH"
export PKG_CONFIG_PATH
PKG_CONFIG_PATH=`echo "$PKG_CONFIG_PATH" | /bin/sed -e 's|/home/bedelman/opt/lalsuite/lib/pkgconfig:||g;'`
PKG_CONFIG_PATH="/home/bedelman/opt/lalsuite/lib/pkgconfig:$PKG_CONFIG_PATH"
