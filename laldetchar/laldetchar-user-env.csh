# source this file to access LALDetChar
if ( ! ${?PYTHONPATH} ) setenv PYTHONPATH
setenv PYTHONPATH `echo "$PYTHONPATH" | /bin/sed -e 's|/home/bedelman/opt/lalsuite/lib/python2.7/site-packages:||g;'`
setenv PYTHONPATH "/home/bedelman/opt/lalsuite/lib/python2.7/site-packages:$PYTHONPATH"
if ( ! ${?MANPATH} ) setenv MANPATH
setenv MANPATH `echo "$MANPATH" | /bin/sed -e 's|/home/bedelman/opt/lalsuite/share/man:||g;'`
setenv MANPATH "/home/bedelman/opt/lalsuite/share/man:$MANPATH"
if ( ! ${?LALDETCHAR_DATADIR} ) setenv LALDETCHAR_DATADIR
setenv LALDETCHAR_DATADIR "/home/bedelman/opt/lalsuite/share/laldetchar"
if ( ! ${?LALDETCHAR_PREFIX} ) setenv LALDETCHAR_PREFIX
setenv LALDETCHAR_PREFIX "/home/bedelman/opt/lalsuite"
if ( ! ${?PATH} ) setenv PATH
setenv PATH `echo "$PATH" | /bin/sed -e 's|/home/bedelman/opt/lalsuite/bin:||g;'`
setenv PATH "/home/bedelman/opt/lalsuite/bin:$PATH"
if ( ! ${?PKG_CONFIG_PATH} ) setenv PKG_CONFIG_PATH
setenv PKG_CONFIG_PATH `echo "$PKG_CONFIG_PATH" | /bin/sed -e 's|/home/bedelman/opt/lalsuite/lib/pkgconfig:||g;'`
setenv PKG_CONFIG_PATH "/home/bedelman/opt/lalsuite/lib/pkgconfig:$PKG_CONFIG_PATH"
