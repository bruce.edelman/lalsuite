# Import SWIG wrappings, if available
from .laldetchar import *

__version__ = "0.3.5.1"

## \addtogroup laldetchar_python
"""This package provides Python wrappings and extensions to LALDetChar"""
