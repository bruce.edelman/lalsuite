%define nightly %{nil}
%define _prefix /usr
%define _mandir %{_prefix}/share/man
%define _sysconfdir %{_prefix}/etc
%define release 1

%if "%{?nightly:%{nightly}}%{!?nightly:0}" == "%{nil}"
%undefine nightly
%endif

Name: laldetchar
Version: 0.3.5.1
Release: %{?nightly:0.%{nightly}}%{!?nightly:%{release}}%{?dist}
Summary: LSC Algorithm DetChar Library
License: GPLv2+
Group: LAL
Source: %{name}-%{version}%{?nightly:-%{nightly}}.tar.xz
URL: http://wiki.ligo.org/DASWG/LALSuite
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildRequires: glib2-devel >= 2.14
BuildRequires: gsl-devel
BuildRequires: libmetaio-devel
BuildRequires: numpy
BuildRequires: octave-devel
BuildRequires: python-devel
BuildRequires: swig >= 3.0.7
BuildRequires: lal-devel >= 6.18.0
BuildRequires: lal-octave >= 6.18.0
BuildRequires: lal-python >= 6.18.0
BuildRequires: lalmetaio-devel >= 1.3.1
BuildRequires: lalmetaio-octave >= 1.3.1
BuildRequires: lalmetaio-python >= 1.3.1
BuildRequires: lalsimulation-devel >= 1.7.0
BuildRequires: lalsimulation-octave >= 1.7.0
BuildRequires: lalsimulation-python >= 1.7.0
BuildRequires: lalburst-devel >= 1.4.4
BuildRequires: lalburst-octave >= 1.4.4
BuildRequires: lalburst-python >= 1.4.4
Requires: glib2
Requires: gsl
Requires: libframe
Requires: libmetaio
Requires: lal >= 6.18.0
Requires: lalmetaio >= 1.3.1
Requires: lalsimulation >= 1.7.0
Requires: lalburst >= 1.4.4
Prefix: %{_prefix}

%description
The LSC Algorithm DetChar Library for gravitational wave data analysis. This
package contains the shared-object libraries needed to run applications
that use the LAL DetChar library.

%package devel
Summary: Files and documentation needed for compiling programs that use LAL DetChar
Group: LAL
Requires: %{name} = %{version}
Requires: glib2-devel >= 2.14
Requires: gsl-devel
Requires: libmetaio-devel
Requires: lal-devel >= 6.18.0
Requires: lalmetaio-devel >= 1.3.1
Requires: lalsimulation-devel >= 1.7.0
Requires: lalburst-devel >= 1.4.4
%description devel
The LSC Algorithm DetChar Library for gravitational wave data analysis. This
package contains files needed build applications that use the LAL DetChar
library.

%package python
Summary: Python Bindings for LALDetChar
Group: LAL
Requires: %{name} = %{version}
Requires: numpy
Requires: python
Requires: lal-python >= 6.18.0
Requires: lalmetaio-python >= 1.3.1
Requires: lalsimulation-python >= 1.7.0
Requires: lalburst-python >= 1.4.4
%description python
The LSC Algorithm Library for gravitational wave data analysis.
This package provides the Python bindings for LALDetChar.

%package octave
Summary: Octave Bindings for LALDetChar
Group: LAL
Requires: %{name} = %{version}
Requires: octave
Requires: lal-octave >= 6.18.0
Requires: lalmetaio-octave >= 1.3.1
Requires: lalsimulation-octave >= 1.7.0
Requires: lalburst-octave >= 1.4.4
%description octave
The LSC Algorithm Library for gravitational wave data analysis.
This package provides the Octave bindings for LALDetChar.

%prep
%setup -q -n %{name}-%{version}%{?nightly:-%{nightly}}

%build
%configure --disable-gcc-flags --enable-swig
%{__make} %{?_smp_mflags} V=1

%check
%{__make} %{?_smp_mflags} V=1 VERBOSE=1 check

%install
%make_install
find $RPM_BUILD_ROOT%{_libdir} -name '*.la' -delete

%post
ldconfig

%postun
ldconfig

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}%{?nightly:-%{nightly}}

%files
%defattr(-,root,root)
%{_bindir}/laldetchar_version
%{_libdir}/*.so.*
%{_sysconfdir}/*

%files devel
%defattr(-,root,root)
%{_includedir}/lal
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*

%files python
%defattr(-,root,root)
%{_bindir}/laldetchar-*
%{_libdir}/python*

%files octave
%defattr(-,root,root)
%{_prefix}/lib*/octave/*/site/oct/*/laldetchar.oct*

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Tue Feb 07 2017 Adam Mercer <adam.mercer@ligo.org> 0.3.5-1
- O2 release

* Mon Sep 26 2016 Adam Mercer <adam.mercer@ligo.org> 0.3.4-1
- ER10 release

* Thu Jun 23 2016 Adam Mercer <adam.mercer@ligo.org> 0.3.3-1
- ER9 release

* Fri Mar 25 2016 Adam Mercer <adam.mercer@ligo.org> 0.3.2-1
- Pre O2 packaging test release
